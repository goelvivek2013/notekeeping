class SharedNote < ActiveRecord::Base
  belongs_to :user
  belongs_to :note
  belongs_to :shareable, polymorphic: true
  belongs_to :sharer, class_name: 'User'
  has_many :shared_notes, as: :shareable, dependent: :destroy
  PERMISSIONS = %w[read update_note owner].freeze
  enum permissions: PERMISSIONS
end
