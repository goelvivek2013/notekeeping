json.extract! shared_note, :id, :user_id, :sharer, :permissions, :shareable_id, :shareable_type, :created_at, :updated_at
json.url shared_note_url(shared_note, format: :json)
